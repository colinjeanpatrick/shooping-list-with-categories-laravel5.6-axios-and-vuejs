<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;

class CategoryController extends Controller
{
  public $rules = [
      'name' => 'required|min:3',
  ];

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $categories = Categories::all();
      $items = Item::all();
      return [ 'items' => $items, 'categories'  => $categories];
  }



  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

      $this->validate($request, $this->rules);

      $category = new Categories();
      $category->name = $request->name;
      $category->save();

      return response()->json($category);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Item  $item
   * @return \Illuminate\Http\Response
   */
  public function show(Category $category)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Item  $item
   * @return \Illuminate\Http\Response
   */
  public function edit(Category $category)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Item  $item
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      $this->validate($request, $this->rules);

      $category = Categories::findOrFail($id);
      $category->name = $request->name;
      $category->updated_at = date('Y-m-d H:i:s');
      $category->save();

      return response()->json($category);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Item  $item
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      $category = Categories::findOrFail($id);
      $category->delete();

      return response()->json($category);
  }

}
