<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public $rules = [
        'name' => 'required|min:3',
        'quantity' => 'required|numeric|min:1',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categories::all();
        $items = Item::all();
        return [ 'items' => $items, 'categories'  => $categories];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, $this->rules);

        $item = new Item();
        $item->category = $request->category;
        $item->name = $request->name;
        $item->quantity = $request->quantity;
        $item->save();

        return response()->json($item);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules);

        $item = Item::findOrFail($id);
        $item->name = $request->name;
        $item->quantity = $request->quantity;
        $item->updated_at = date('Y-m-d H:i:s');
        $item->save();

        return response()->json($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::findOrFail($id);
        $item->delete();

        return response()->json($item);
    }
    /**
     * Remove the all resources from a category.
     *
     * @param  \App\Category->id  $cat_id
     * @return \Illuminate\Http\Response
     */
     public function clearCategory($cat_id)
     {
         $deleted_items = array();
         $items = Item::all()->where('category', "=", $cat_id);
         foreach ($items as $item) {
            $item->delete();
            $deleted_items[] = $item->id;
         }
         return response()->json($deleted_items);
     }
     /**
      * Remove the all resources from a category.
      *
      * @param  \App\Category->id  $cat_id
      * @return \Illuminate\Http\Response
      */
      public function clearAllItems()
      {
          $deleted_items = array();
          $items = Item::all();
          foreach ($items as $item) {
             $item->delete();
             $deleted_items[] = $item->id;
          }
          return response()->json($deleted_items);
      }
}
