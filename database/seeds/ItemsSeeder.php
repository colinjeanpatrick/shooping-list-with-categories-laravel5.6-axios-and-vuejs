<?php

use Illuminate\Database\Seeder;

class ItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('items')->insert([
        'name' => 'Coca-cola',
        'category' => 3,
        'quantity' => 2,
      ]);
      DB::table('items')->insert([
        'name' => 'Biéres',
        'category' => 3,
        'quantity' => 1,
      ]);
      DB::table('items')->insert([
        'name' => 'Céréales pour le petit dej',
        'category' => 1,
        'quantity' => 1,
      ]);
      DB::table('items')->insert([
        'name' => 'Bananes',
        'category' => 4,
        'quantity' => 4,
      ]);
      DB::table('items')->insert([
          'name' => 'Poivre',
          'category' => 5,
          'quantity' => 1,
      ]);
    }
}
