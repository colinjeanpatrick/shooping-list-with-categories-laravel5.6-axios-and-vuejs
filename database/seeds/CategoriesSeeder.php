<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('categories')->insert([
          'name' => 'Céréales et féculents',
        ]);
        DB::table('categories')->insert([
          'name' => 'Surgelés',
        ]);
        DB::table('categories')->insert([
          'name' => 'Boissons',
        ]);
        DB::table('categories')->insert([
          'name' => 'fruits et Légumes',
        ]);
        DB::table('categories')->insert([
            'name' => 'Épices',
        ]);
        DB::table('categories')->insert([
          'name' => 'Viandes, poisson',
        ]);

    }
}
